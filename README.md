## Styles Object to CSS String -- Client Side

Simple script to create a css file from a stylesObject (json) and trigger a download.

### Usage:

- Add the contents of `index.js` to the theme editor page using a content component
- Trigger the `createCssFileDownload` function on form submit

```js
createCssFileDownload(styleObj, "customer.css");
```
