/*
 * Unqork Styles Object to CSS String
 * Created by Design Services
 *
 * Simple script to convert a JSON object with CSS property values into a CSS String.
 * Create CSS file client-side and trigger file download
 */

/*
 * Functions to handle Styles Object to CSS String
 * Source: https://www.npmjs.com/package/style-object-to-css-string
 */

// Funtion to create parsers
function createParser(matcher, replacer) {
  const regex = RegExp(matcher, "g");
  return (string) => {
    // * throw an error if not a string
    if (typeof string !== "string") {
      throw new TypeError(
        `expected an argument of type string, but got ${typeof styleObj}`
      );
    }

    // * if no match between string and matcher
    if (!string.match(regex)) {
      return string;
    }

    // * executes the replacer function for each match
    // ? replacer can take any arguments valid for String.prototype.replace
    return string.replace(regex, replacer);
  };
}

// Parser Instances
const camelToKebab = createParser(
  /[A-Z]/,
  (match) => `-${match.toLowerCase()}`
);
const snakeToKebab = createParser(/_/, () => "-");

// Object to String Function
function objToString(styleObj, parser = camelToKebab) {
  if (!styleObj || typeof styleObj !== "object" || Array.isArray(styleObj)) {
    throw new TypeError(
      `expected an argument of type object, but got ${typeof styleObj}`
    );
  }
  const lines = Object.keys(styleObj).map(
    (property) => `${parser(property)}: ${styleObj[property]};`
  );
  return lines.join("\n");
}

/*
 * Function to Create CSS File and Download
 */

function createCssFileDownload(styleObj, fileName = "customer.css") {
  // Create CSS String
  let stylesString = `:root{ ${objToString(styleObj)} }`;

  // Create CSS File
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/css;charset=utf-8," + encodeURIComponent(stylesString)
  );
  element.setAttribute("download", fileName);
  element.style.display = "none";
  document.body.appendChild(element);

  // Trigger Download
  element.click();
  document.body.removeChild(element);
}
